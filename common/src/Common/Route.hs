{-# LANGUAGE EmptyCase #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
module Common.Route where

{- -- You will probably want these imports for composing Encoders.
import Prelude hiding (id, (.))
import Control.Category
-}

import Data.Text (Text)
import Data.Functor.Identity
import Data.Some (Some)
import qualified Data.Some as Some
import Data.Dependent.Sum (DSum ((:=>)))

import Obelisk.Route
import Obelisk.Route.TH

data BackendRoute :: * -> * where
  -- | Used to handle unparseable routes.
  BackendRoute_Missing :: BackendRoute ()
  BackendRoute_Compile :: BackendRoute ()
  -- You can define any routes that will be handled specially by the backend here.
  -- i.e. These do not serve the frontend, but do something different, such as serving static files.

data FrontendRoute :: * -> * where
  FrontendRoute_Main :: FrontendRoute ()
  FrontendRoute_Naproche :: FrontendRoute Text
  FrontendRoute_Nave :: FrontendRoute Text
  -- This type is used to define frontend routes, i.e. ones for which the backend will serve the frontend.

-- | Provide a human-readable name for a given section
sectionTitle :: Some FrontendRoute -> Text
sectionTitle (Some.Some sec) = case sec of
  FrontendRoute_Main -> "Naproche"
  FrontendRoute_Naproche -> "Try Naproche"
  FrontendRoute_Nave -> "Try Nave"

-- | Provide a human-readable name for a route
routeTitle :: R FrontendRoute -> Text
routeTitle (sec :=> _) = sectionTitle $ Some.Some sec

-- | Provide a human-readable description for a given section
sectionDescription :: Some FrontendRoute -> Text
sectionDescription (Some.Some sec) = case sec of
  FrontendRoute_Main -> "Naproche: Natural proof checking"
  FrontendRoute_Naproche -> "Try Naproche in a webeditor"
  FrontendRoute_Nave -> "Try Nave in a webeditor"

-- | Provide a human-readable description for a given route
routeDescription :: R FrontendRoute -> Text
routeDescription (sec :=> _) = sectionDescription $ Some.Some sec

-- | Given a section, provide its default route
sectionHomepage :: Some FrontendRoute -> R FrontendRoute
sectionHomepage (Some.Some sec) = sec :/ case sec of
  FrontendRoute_Main -> ()
  FrontendRoute_Nave -> ""
  FrontendRoute_Naproche -> ""

fullRouteEncoder
  :: Encoder (Either Text) Identity (R (FullRoute BackendRoute FrontendRoute)) PageName
fullRouteEncoder = mkFullRouteEncoder
  (FullRoute_Backend BackendRoute_Missing :/ ())
  (\case
      BackendRoute_Missing -> PathSegment "missing" $ unitEncoder mempty
      BackendRoute_Compile -> PathSegment "ws_compile" $ unitEncoder mempty)
  (\case
      FrontendRoute_Main -> PathEnd $ unitEncoder mempty
      FrontendRoute_Nave -> PathSegment "try-nave" $ singlePathSegmentEncoder
      FrontendRoute_Naproche -> PathSegment "try-naproche" $ singlePathSegmentEncoder)

concat <$> mapM deriveRouteComponent
  [ ''BackendRoute
  , ''FrontendRoute
  ]
