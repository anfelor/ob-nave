{-# LANGUAGE ScopedTypeVariables, DeriveGeneric #-}
{-# OPTIONS -Wno-orphans #-}

module Common.Message where

import           Data.Aeson (ToJSON, FromJSON, toEncoding, parseJSON,
                            defaultOptions, Options,
                            genericToEncoding, genericParseJSON)
import Data.Text (Text)
import           GHC.Generics (Generic)
import System.Exit (ExitCode)

instance ToJSON ExitCode where toEncoding = genericToEncoding options
instance FromJSON ExitCode where parseJSON = genericParseJSON options

data C2S = C2SgetLean Text
         | C2SrunLean Text
         | C2SgetPdf Text
         | C2Snaproche Text
         deriving (Eq,Show, Generic)

options :: Options
options = defaultOptions -- { tagSingleConstructors = True }

instance ToJSON C2S where toEncoding = genericToEncoding options
instance FromJSON C2S where parseJSON = genericParseJSON options

data S2C = S2Cnave (Either Text Text)
         | S2Cpdf (Either Text Text)
         | S2Cnaproche [Text]
         | S2CnaprocheDone ExitCode
         deriving (Eq,Show, Generic)

instance ToJSON S2C where toEncoding = genericToEncoding options
instance FromJSON S2C where parseJSON = genericParseJSON options
