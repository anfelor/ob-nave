{-# LANGUAGE OverloadedStrings #-}

module Common.Examples where

import Data.Text (Text)

primeNoSquare :: Text
primeNoSquare = 
     "\\begin{definition}\n"
  <> "  A natural number $p$ is prime iff $p\\neq 0,1$ and for every $k$\n"
  <> "  such that $k\\divides p$, we have $k = p$ or $k = 1$.\n"
  <> "\\end{definition}\n\n"
  <> "\\begin{theorem}[Euclids lemma]\n"
  <> "  If $p$ is prime and $p\\divides m\\mul n$\n"
  <> "  then $p\\divides m$ or $p\\divides n$.\n"
  <> "\\end{theorem}"