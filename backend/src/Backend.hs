{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Backend where

import Common.Route
import Obelisk.Backend
import Data.Dependent.Sum (DSum (..))
import Data.Functor.Identity
import Control.Concurrent
import Network.WebSockets.Snap

import           Control.Concurrent (MVar)
import           Control.Monad      (forever, join)
import           Data.Aeson         (encode, decode)
import qualified Data.ByteString    as B
import           Data.ByteString.Lazy (toStrict)
import qualified Data.Text          as T
import qualified Data.Text.Lazy     as L
import qualified Data.Text.IO       as T
import qualified Data.Text.Encoding as E
import qualified Network.WebSockets as WS
import           Control.Exception
import           System.Exit (ExitCode(..))
import           GHC.IORef
import qualified Data.Map as Map

import qualified Obelisk.ExecutableConfig.Lookup as Cfg
import Common.Message hiding (options)

import SAD.API
import qualified SAD.Main as Naproche
import qualified Nave
import qualified Extern

backend :: Backend BackendRoute FrontendRoute
backend = Backend
  { _backend_run = \serve -> do
      m <- Cfg.getConfigs 
      proversYaml <- evaluate $ m Map.! "backend/provers.yaml"
      state <- newMVar $ newServerState proversYaml
      serve $ \case
        BackendRoute_Missing :=> Identity () -> return ()
        BackendRoute_Compile :=> Identity () -> do
          runWebSocketsSnap (application state)

  , _backend_routeEncoder = fullRouteEncoder
  }

type ServerState = B.ByteString -- Dummy

newServerState :: ServerState -> ServerState
newServerState = id

application :: MVar ServerState -> WS.ServerApp
application state pending = do
    conn <- WS.acceptRequest pending
    WS.forkPingThread conn 30
    talk conn state

-- | TODO: The server blocks during compiles.
-- The Naproche code should be in a separate thread
-- and killed when a new request comes.
talk :: WS.Connection -> MVar ServerState -> IO ()
talk conn state = forever $ do
  proversYaml <- readMVar state
  msgbs <- WS.receiveData conn :: IO B.ByteString
  case decode $ WS.toLazyByteString msgbs of
    Nothing -> do
      T.putStrLn "\nCouldn't decode message\n"
    Just (C2Snaproche txt) -> do
      oldProofTextRef <- newIORef $ ProofTextRoot []
      initThread [] $ \bs -> do
        WS.sendTextData conn $ toStrict $ encode $ S2Cnaproche $ fmap E.decodeUtf8 bs
      (Naproche.mainBody (Just proversYaml) oldProofTextRef ([], [ProofTextInstr noPos (GetArgument Text (L.fromStrict txt))])) 
        `catches` 
        [ Handler $ \(e :: ExitCode) -> do
          WS.sendTextData conn $ toStrict $ encode $ S2CnaprocheDone e
        , Handler $ \(e :: SomeException) -> do
          WS.sendTextData conn $ toStrict $ encode $ S2Cnaproche [T.pack (displayException e)]
          WS.sendTextData conn $ toStrict $ encode $ S2CnaprocheDone (ExitFailure 1)
        ]
    Just (C2SgetLean txt) -> do
      l <- Nave.compileLean "" txt
      let result = case l of
              Left e -> Left $ T.pack e
              Right t -> Right t
      WS.sendTextData conn $ (toStrict . encode . S2Cnave) result
    Just (C2SrunLean txt) -> do
      l <- Nave.compileLean "" txt
      result <- case l of
        Left e -> pure $ Left $ T.pack e
        Right t -> catchAll $ Extern.runLean t
      WS.sendTextData conn $ (toStrict . encode . S2Cnave) result
    Just (C2SgetPdf txt) -> do
      result <- catchAll $ Extern.runLatex txt
      WS.sendTextData conn $ (toStrict . encode . S2Cpdf) result

catchAll :: IO (Either T.Text a) -> IO (Either T.Text a)
catchAll cmd = (join . either (Left . T.pack . (displayException :: SomeException -> String)) Right) 
  <$> try (cmd >>= evaluate)