{ mkDerivation, aeson, base, bytestring, common, containers
, dependent-sum, Earley, frontend, megaparsec, Naproche-SAD, nave
, obelisk-backend, obelisk-executable-config-lookup, obelisk-route
, pretty-simple, stdenv, text, time, websockets, websockets-snap
}:
mkDerivation {
  pname = "backend";
  version = "0.1";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson base bytestring common containers dependent-sum Earley
    frontend megaparsec Naproche-SAD nave obelisk-backend
    obelisk-executable-config-lookup obelisk-route pretty-simple text
    time websockets websockets-snap
  ];
  executableHaskellDepends = [
    base common frontend obelisk-backend
  ];
  license = "unknown";
  hydraPlatforms = stdenv.lib.platforms.none;
}
