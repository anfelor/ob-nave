{ obelisk ? import ./.obelisk/impl {
    system = builtins.currentSystem;
    iosSdkVersion = "10.2";
    config.android_sdk.accept_license = true;
  }
}:
with obelisk;
project ./. ({ pkgs, ... }: {
  android.applicationId = "systems.naproche";
  android.displayName = "Naproche";
  ios.bundleIdentifier = "systems.naproche";
  ios.bundleName = "Naproche";
  packages = {
    nave = ./nave;
    Naproche-SAD = ./Naproche-SAD;
    extern = ./extern;
  };
  overrides = self: super:
  {
    Naproche-SAD = self.callPackage ./Naproche-SAD {
      eprover = pkgs.eprover;
    };
    extern = self.callPackage ./extern {
      lean = pkgs.lean;
      pdflatex = pkgs.texlive.combined.scheme-medium;
    };
  };
})
