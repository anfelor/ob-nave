{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Frontend where

import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Fix
import Data.Text (Text)
import qualified Data.Text          as T
import qualified Data.Text.Lazy     as L
import qualified Data.Aeson as Aeson
import qualified Data.ByteString    as B
import           Data.ByteString.Lazy (toStrict, fromStrict)
import           Text.URI
import           Data.List.NonEmpty (nonEmpty)
import qualified Data.Some as Some
import Data.Dependent.Sum (DSum ((:=>)))
import Data.Universe (universe)
import qualified Data.Map as Map
import Data.Text.Encoding as E
import System.Exit (ExitCode(..))
import Control.Exception
import           GHC.IORef
import           Control.Concurrent

import Obelisk.Frontend
import Obelisk.Route
import Obelisk.Route.Frontend
import Obelisk.Generated.Static
import qualified Obelisk.Configs as Cfg

import Reflex.Dom.Core

import Common.Route
import Common.Message (S2C(..), C2S(..))
import Common.Examples (primeNoSquare)

import SAD.API
import qualified SAD.Main as Naproche

-- This runs in a monad that can be run on the client or the server.
-- To run code in a pure client or pure server context, use one of the
-- `prerender` functions.
frontend :: Frontend (R FrontendRoute)
frontend = Frontend
  { _frontend_head = do
      el "title" $ text "Nave"
      elAttr "link" ("rel" =: "stylesheet" <> "href" =: "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" 
                    <> "integrity" =: "sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
                    <> "crossorigin" =: "anonymous") blank
      elAttr "link" ("rel" =: "stylesheet" <> "href" =: static @"codemirror-5.57.0/lib/codemirror.css") blank
      elAttr "link" ("href" =: static @"cover.css" <> "type" =: "text/css" <> "rel" =: "stylesheet") blank
      elAttr "link" ("href" =: static @"main.css" <> "type" =: "text/css" <> "rel" =: "stylesheet") blank

      elAttr "script" ("src" =: "https://code.jquery.com/jquery-3.5.1.slim.min.js" <> "integrity" =: "sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" <> "crossorigin" =: "anonymous") blank
      elAttr "script" ("src" =: "https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" <> "integrity" =: "sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" <> "crossorigin" =: "anonymous") blank
      elAttr "script" ("src" =: "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" <> "integrity" =: "sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" <> "crossorigin" =: "anonymous") blank

      elAttr "script" ("src" =: static @"codemirror-5.57.0/lib/codemirror.js") blank
  , _frontend_body = do
      liftIO $ compile "Let x be a set." (\x -> pure ())
      m <- Cfg.getConfigs 
      let r = E.decodeUtf8 <$> Map.lookup "common/route" m
      elAttr "div" ("class" =: "cover-container d-flex w-100 h-100 mx-auto flex-column") $ do
        header
        elAttr "main" ("role" =: "main" <> "class" =: "inner cover") $ do
          subRoute_ $ \case
            FrontendRoute_Main -> aboutPage
            FrontendRoute_Nave -> playground r $ \result exit -> do
              adjacent (updatedRoute FrontendRoute_Nave codeArea) $
                \code -> resultArea (naveExamples m) FrontendRoute_Nave result exit $ do
                    (getPdf, _) <- elAttr' "button" ("class" =: "btn btn-primary" <> "style" =: "float: right") $ do
                      text "Get PDF"
                    (runLean, _) <- elAttr' "button" ("class" =: "btn btn-primary" <> "style" =: "float: right") $ do
                      text "Run Lean"
                    (getLean, _) <- elAttr' "button" ("class" =: "btn btn-primary" <> "style" =: "float: right") $ do
                      text "Get Lean"
                    pure $ leftmost $ map (\(c2s, b) -> fmap c2s . tag (current code) . domEvent Click $ b) 
                      [(C2SgetLean, getLean), (C2SrunLean, runLean), (C2SgetPdf, getPdf)]
            FrontendRoute_Naproche -> playground r $ \result exit -> do
              adjacent (updatedRoute FrontendRoute_Naproche codeArea) $
                \code -> resultArea (naprocheExamples m) FrontendRoute_Naproche result exit $ do
                    (e, _) <- elAttr' "button" ("class" =: "btn btn-primary" <> "style" =: "float: right") $ text "Run with eprover"
                    pure $ fmap C2Snaproche $ tag (current code) (domEvent Click e)

        elAttr "footer" ("class" =: "mastfoot mt-auto") $ do
          elAttr "div" ("class" =: "inner") $ do
            el "p" $ text "Made with Bootstrap and Reflex FRP"
      return ()
  }

header :: (MonadHold t m, DomBuilder t m, MonadFix m, PostBuild t m
  , Routed t (R FrontendRoute) m
  , SetRoute t (R FrontendRoute) m
  , RouteToUrl (R FrontendRoute) m
  , Prerender js0 t m) => m () 
header = do
  elAttr "header" ("class" =: "masthead") $ do
    elAttr "div" ("class" =: "inner") $ do
      elAttr "h3" ("class" =: "masthead-brand") $ routeLink (FrontendRoute_Main :/ ()) $ text "Naproche"
      elAttr "nav" ("class" =: "nav nav-masthead justify-content-center") $ mdo

      currentTab <- askRoute
      let currentTabDemux = demux $ fmap (\(sec :=> _) -> Some.Some sec) currentTab
      forM_ (filter (/= (Some.Some FrontendRoute_Main)) universe) $ \section -> do
        let thisTabIsSelected = demuxed currentTabDemux section
            highlight = ffor thisTabIsSelected $ \case
              True -> "class" =: "nav-link active"
              False -> "class" =: "nav-link"
        routeLink (sectionHomepage section) $ elDynAttr "span" highlight $ text $ sectionTitle section

naprocheExamples :: Map.Map Text B.ByteString -> [(Text, Text)]
naprocheExamples m = fmap (fmap E.decodeUtf8)
  [ ("Chinese", m Map.! "common/examples/naproche/chinese.ftl")
  , ("Fürsts Lemma", m Map.! "common/examples/naproche/fuerst.ftl")
  , ("Koenigs Lemma", m Map.! "common/examples/naproche/Koenigs_lemma.ftl")
  , ("Maximum principle", m Map.! "common/examples/naproche/Maximum_principle.ftl")
  , ("Powerset", m Map.! "common/examples/naproche/powerset.ftl")
  , ("Prime no square", m Map.! "common/examples/naproche/prime_no_square.ftl")
  , ("Regular successor", m Map.! "common/examples/naproche/regular_successor.ftl")
  ]

naveExamples :: Map.Map Text B.ByteString -> [(Text, Text)]
naveExamples m = fmap (fmap E.decodeUtf8)
  [ ("Primes are not squares", m Map.! "common/examples/nave/prime-not-square.tex")
  -- , ("Tarski", m Map.! "common/examples/nave/tarski.tex")
  ]

codeArea :: (DomBuilder t m) => Text -> Event t Text -> m (Dynamic t Text)
codeArea initialText updatedText = do
  tae <- textAreaElement $ def
    & textAreaElementConfig_initialValue .~ initialText
    & textAreaElementConfig_setValue .~ updatedText
    & textAreaElementConfig_elementConfig . elementConfig_initialAttributes 
      .~ ("id" =: "code-area" <> "class" =: "form-control" <> "rows" =: "10" <> "style" =: "font-family: monospace")
  pure $ _textAreaElement_value tae

data Result = ShowExamples | Output Text | PdfLink Text
  deriving (Eq, Show)

instance Semigroup Result where
  Output a <> Output b = Output (a <> b)
  _ <> b = b

resultArea :: (DomBuilder t m, RouteToUrl (R FrontendRoute) m, SetRoute t (R FrontendRoute) m, Prerender js0 t m) 
  => [(Text, Text)] -> FrontendRoute Text -> Result -> ExitCode -> m a -> m a
resultArea examples route result exit actions = do
  res <- el "div" $ do
    elAttr "button" ("class" =: "btn" <> "style" =: ("color: " <> case exit of ExitSuccess -> "green"; _ -> "red"))
        $ text $ case exit of ExitSuccess -> "Success"; _ -> "Failed"
    actions
  case result of
    ShowExamples -> do
      elAttr "div" ("class" =: "examples") $ do
        el "h3" $ text "Some examples:"
        el "ul" $ forM_ examples $ \(title, content) -> do 
          routeLink (route :/ content) $ 
            el "li" $ do
              text title
    Output txt -> do
      elAttr "pre" ("id" =: "result-area") $ do
        text txt
    PdfLink txt -> do
      elAttr "object" ("width" =: "100%" <> "height" =: "100%" <> "data" =: txt <> "type" =: "application/pdf" <> "class" =: "internal") $ 
        elAttr "embed" ("src" =: txt <> "type" =: "application/pdf") $ blank
          -- text "Open pdf in new tab"
  pure res

adjacent :: DomBuilder t m => m a -> (a -> m b) -> m b
adjacent left right = do
  l <- elAttr "div" ("id" =: "left-area") $ left
  r <- elAttr "div" ("id" =: "right-area") $ right l
  pure r

updatedRoute :: (MonadFix m, MonadHold t m, Routed t Text m, SetRoute t (R FrontendRoute) m, MonadSample t m)
  => FrontendRoute Text -> (Text -> Event t Text -> m (Dynamic t Text)) -> m (Dynamic t Text)
updatedRoute route widget = mdo
  r <- askRoute
  initialText <- sample $ current r
  currentText <- widget initialText (updated r)
  updatedText <- holdDyn initialText $ fmapMaybe 
    (\(old, new) -> if new /= old then Just new else Nothing)
    (attach (current updatedText) (updated currentText))
  setRoute $ (\a -> route :/ a) <$> updated updatedText
  pure currentText

aboutPage :: DomBuilder t m => m ()
aboutPage = do
  elAttr "div" ("class" =: "main-page") $ do
    elAttr "div" ("class" =: "banner row") $ do
      elAttr "div" ("class" =: "banner-left col-md") $
        elAttr "pre" ("class" =: "code-sample") $ 
          text primeNoSquare
      elAttr "div" ("class" =: "banner-right col-md") $ do
        el "h1" $ text "Natural Proof Checking"
        el "p" $ text "Formalise mathematics the way you write it"
    elAttr "div" ("class" =: "") $ do
      el "h2" $ text "Resources"
      elAttr "div" ("class" =: "card-deck") $ do
        elAttr "div" ("class" =: "card") $ do
          elAttr "iframe" ("src" =: "https://www.youtube.com/embed/rsxzHFxa424" 
            <> "class" =: "card-img-top"
            <> "allow" =: "accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            <> "allowfullscreen" =: "") $ blank
          elAttr "div" ("class" =: "card-body") $ do
            elAttr "h5" ("class" =: "card-title") $ do
              text "Lean Together 2020"
        elAttr "div" ("class" =: "card") $ do
          elAttr "div" ("class" =: "card-body") $ do
            elAttr "h5" ("class" =: "card-title") $ do
              text "FLib"
            elAttr "p" ("class" =: "card-text") $ 
              text "FLib is our collection of formalised mathematical texts."
            elAttr "a" ("class" =: "card-link" <> "href" =: "https://github.com/naproche-community/FLib") $ text "Github"
        elAttr "div" ("class" =: "card") $ do
          elAttr "div" ("class" =: "card-body") $ do
            elAttr "h5" ("class" =: "card-title") $ do
              text "Naproche-SAD"
            elAttr "p" ("class" =: "card-text") $ 
              text ""
            elAttr "a" ("class" =: "card-link" <> "href" =: "https://github.com/anfelor/Naproche-SAD") $ text "Github"
        elAttr "div" ("class" =: "card") $ do
          elAttr "div" ("class" =: "card-body") $ do
            elAttr "h5" ("class" =: "card-title") $ do
              text "Nave"
            elAttr "p" ("class" =: "card-text") $ 
              text ""
            elAttr "a" ("class" =: "card-link" <> "href" =: "https://github.com/adelon/nave") $ text "Github"
    elAttr "div" ("class" =: "") $ do
      el "h2" $ text "News"
      el "ul" $ do
        el "li" $ elAttr "a" ("href" =: "http://aitp-conference.org/2020/abstract/paper_16.pdf") $ text "Naproche at AITP"

playground
  :: ( DomBuilder t m
     , MonadFix m
     , MonadHold t m
     , PostBuild t m
     , PerformEvent t m
     , TriggerEvent t m
     , Prerender js t m
     , Routed t Text m
     , SetRoute t (R FrontendRoute) m
     )
  => Maybe Text -> (Result -> ExitCode -> m (Event t C2S)) -> m ()
playground r widget = do
  rec
    msgSendEv <- switchDyn <$> widgetHold (widget ShowExamples ExitSuccess) 
      (uncurry widget <$> eRecRespEv)
    let
      wsRespEv = switch (current wsRespDyn)
      msgRecEv = fmapMaybe decodeOneMsg wsRespEv
      eRecRespEv = updated (zipDyn eRecRespTxt eRecRespExit)
    eRecRespTxt <- holdDyn ShowExamples $ leftmost [const (Output "") <$> msgSendEv, 
      attachWith (<>) (current eRecRespTxt) (fmapMaybe showMsg msgRecEv)]
    eRecRespExit <- holdDyn ExitSuccess (fmapMaybe getExit msgRecEv)
    wsRespDyn <- prerender (return never) $ do
      case checkEncoder fullRouteEncoder of
        Left err -> do
          elAttr "div" ("class" =: "debug") $ text err
          return never
        Right encoder -> do
          let wsPath = fst $ encode encoder $ hoistR FullRoute_Backend $ BackendRoute_Compile :/ ()
              sendEv = fmap ((:[]) . toStrict . Aeson.encode) msgSendEv
          let mUri = do
                uri' <- mkURI =<< r
                pathPiece <- nonEmpty =<< mapM mkPathPiece wsPath
                wsScheme <- case uriScheme uri' of
                  rtextScheme | rtextScheme == mkScheme "https" -> mkScheme "wss"
                  rtextScheme | rtextScheme == mkScheme "http" -> mkScheme "ws"
                  _ -> Nothing
                return $ uri'
                  { uriPath = Just (False, pathPiece)
                  , uriScheme = Just wsScheme
                  }
          case mUri of
            Nothing -> do
              return never
            Just uri -> do
              ws <- webSocket (render uri) $ def & webSocketConfig_send .~ sendEv
              return (_webSocket_recv ws)
  blank
  where
    decodeOneMsg :: B.ByteString -> Maybe S2C
    decodeOneMsg = Aeson.decode . fromStrict

    getExit :: S2C -> Maybe ExitCode
    getExit = \case
      (S2Cnave (Left _)) -> Just $ ExitFailure 1
      (S2Cnave (Right _)) -> Just $ ExitSuccess
      (S2Cpdf (Left _)) -> Just $ ExitFailure 1
      (S2Cpdf (Right _)) -> Just $ ExitSuccess
      (S2Cnaproche _) -> Nothing
      (S2CnaprocheDone e) -> Just e

    showMsg :: S2C -> Maybe Result
    showMsg = \case
      (S2Cnave (Left l)) -> Just $ Output l
      (S2Cnave (Right l)) -> Just $ Output l
      (S2Cpdf (Left l)) -> Just $ Output l
      (S2Cpdf (Right l)) -> Just $ PdfLink $ "data:application/pdf;base64," <> l
      (S2Cnaproche txt) -> Just $ Output $ T.unlines txt
      (S2CnaprocheDone _) -> Nothing

compile :: Text -> (S2C -> IO ()) -> IO ()
compile txt send = do
  oldProofTextRef <- newIORef $ ProofTextRoot []
  initThread [] $ \bs -> do
    send $ S2Cnaproche $ fmap E.decodeUtf8 bs
  (Naproche.mainBody (Just proversYaml) oldProofTextRef ([], [ProofTextInstr noPos (GetArgument Text (L.fromStrict txt))])) 
  `catches` 
  [ Handler $ \(e :: ExitCode) -> do
     send $ S2CnaprocheDone e
  , Handler $ \(e :: SomeException) -> do
     send $ S2Cnaproche [T.pack (displayException e)]
     send $ S2CnaprocheDone (ExitFailure 1)
  ]

proversYaml :: B.ByteString
proversYaml = foldl (\a b -> a <> "\n" <> b) ""
  [ "---"
  , "## Naproche-SAD's provers"

  , "- name: \"eprover\""
    , "label: \"eprover\""
    , "path: \"eprover\""
    , "arguments: "
      , "- \"--auto\""
      , "- \"-s\""
      , "- \"--memory-limit=6144\""
      , "- \"--cpu-limit=%d\""
    , "successMessage:"
      , "- \"# SZS status Theorem\""
    , "contradictionMessage:"
      , "- \"# SZS status ContradictoryAxioms\""
    , "failureMessage:"
      , "- \"# SZS status CounterSatisfiable\""
    , "unknownMessage:"
      , "- \"# SZS status ResourceOut\""
      , "- \"# SZS status GaveUp\""

  , "# Verbose provers"

  , "- name: \"eproververb\""
    , "label: \"eproververb\""
    , "path: \"eprover\""
    , "arguments: "
      , "- \"-xAuto\""
      , "- \"-tAuto\""
      , "- \"-mAuto\""
      , "- \"--tstp-in\""
      , "- \"-l\""
      , "- \"2\""
      , "- \"--cpu-limit=%d\""
    , "successMessage:"
      , "- \"# SZS status Theorem\""
    , "contradictionMessage:"
      , "- \"# SZS status ContradictoryAxioms\""
    , "failureMessage:"
      , "- \"# SZS status CounterSatisfiable\""
    , "unknownMessage:"
      , "- \"# SZS status ResourceOut\""
      , "- \"# SZS status GaveUp\""

  , "- name: \"spass\""
    , "label: \"SPASS\""
    , "path: \"SPASS\""
    , "arguments:"
      , "- \"-TPTP\""
      , "- \"-CNFOptSkolem=0\""
      , "- \"-PProblem=0\""
      , "- \"-PGiven=0\""
      , "- \"-Stdin\""
      , "- \"-TimeLimit=%d\""
    , "successMessage:"
      , "- \"SPASS beiseite: Proof found.\""
    , "contradictionMessage: []"
    , "failureMessage:"
      , "- \"SPASS beiseite: Completion found.\""
    , "unknownMessage:"
      , "- \"SPASS beiseite: Ran out of time.\""
  , "#   - \"SPASS beiseite: Maximal number of loops exceeded.\""

  , "- name: \"vampire\""
    , "label: \"vampire\""
    , "path: \"vampire4.2.2\""
    , "arguments:"
      , "- \"--mode\""
      , "- \"casc\""
        , "#  - \"-t %d\""
    , "successMessage:"
      , "- \"% SZS output end Proof for\""
    , "contradictionMessage: []"
      , "# guessed this one"
      , "#  - \"% SZS status ContradictoryAxioms for\""
    , "failureMessage:"
      , "- \"% SZS status CounterSatisfiable for\""
    , "unknownMessage:"
      , "- \"% SZS status Timeout for\""
  ]