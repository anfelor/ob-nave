{ mkDerivation, base, bytestring, process, stdenv, text, hashable, directory, aeson, filepath, base64-bytestring, lean, pdflatex }:
mkDerivation {
  pname = "extern";
  version = "0.1";
  src = ./.;
  libraryHaskellDepends = [ base bytestring process text hashable directory aeson filepath base64-bytestring ];
  librarySystemDepends = [ lean pdflatex ];
  license = "unknown";
  hydraPlatforms = stdenv.lib.platforms.none;
}
