{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Extern (runLean, runLatex) where

import Control.Monad
import Data.Aeson
import qualified Data.ByteString as B
import qualified Data.ByteString.Base64 as Base64
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Encoding as E
import qualified Data.Text.IO as T
import Data.Hashable (hash)
import Data.Maybe
import Data.String
import GHC.Generics
import GHC.IO.Encoding
import System.Directory
import System.Exit (ExitCode(..))
import System.FilePath
import System.Process (readProcessWithExitCode)

data LeanOutput = LeanOutput
  { caption :: Text
  , file_name :: Text
  , pos_col :: Int
  , pos_line :: Int
  , severity :: Text
  , text :: Text
  } deriving (Eq, Show, Generic)

instance FromJSON LeanOutput

runLean :: Text -> IO (Either Text Text)
runLean t = do
  setLocaleEncoding utf8
  let file = "tmp_" <> show (hash t) <> ".lean"
  T.writeFile file t
  (e, stdout, _) <- readProcessWithExitCode "lean" ["--json", file] ""
  removeFile file
  let res = T.unlines $ fmap (text . fromJust . decode . fromString) $ lines stdout
  case e of
    ExitSuccess -> pure $ Right res
    ExitFailure _ -> pure $ Left res

preamble :: Text
preamble = "\\documentclass{article}\n\\usepackage[utf8]{inputenc}\n\\usepackage[english]{babel}\n\\usepackage{amsthm}\n\\theoremstyle{definition}\n\\newtheorem{definition}{Definition}\n\\newtheorem{theorem}{Theorem}\n\\newcommand{\\divides}{\\mid}\n\\newcommand{\\mul}{\\cdot}\n\\begin{document}\n"

postamble :: Text
postamble = "\\end{document}"

runLatex :: Text -> IO (Either Text Text)
runLatex t = do
  setLocaleEncoding utf8
  let file = "tmp_" <> show (hash t) <> ".tex"
  T.writeFile file $ preamble <> t <> postamble
  (e, stdout, _) <- readProcessWithExitCode "pdflatex" ["-halt-on-error", file] ""
  result <- case e of
    ExitSuccess -> do 
      pdf <- Base64.encode <$> B.readFile (file -<.> "pdf")
      pure $ Right $ E.decodeUtf8 pdf
    ExitFailure _ -> pure $ Left $ T.pack stdout
  forM_ [file, file -<.> "pdf", file -<.> "log", file -<.> "aux"] $ \f -> do
    exists <- doesFileExist f
    when exists $ removeFile f
  pure result