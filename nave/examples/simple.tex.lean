
definition almost_all_nat (p : ℕ  -> Prop) := ∃ l, ∀ n : ℕ, n ≤ l → p n
notation `∀∞` binders `, ` r:(scoped P, almost_all_nat P) := r

notation `natural_number` := ℕ
notation `rational_number` := ℕ
def divides {α} := @has_dvd.dvd α
def neq {α} (a : α) (b) := a ≠ b
def mul {α} := @has_mul.mul α


axiom ax_0 {C} {B} {A}
  : lor A (land B C) → land (lor A B) (lor A C)