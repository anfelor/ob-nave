[synonym word/-s]
[synonym number/-s]
[synonym formula/-s]


Let (u,v,w) stand for (u,(v,w)).
Let (u,v,w,x) stand for (u,v,(w,x)).
Let (u,v,w,x,y) stand for (u,v,w,(x,y)).
Let (u,v,w,x,y,z) stand for (u,v,w,x,(y,z)).

%
% Words
%

Signature. A word is a notion.

Signature. [] is a word.
Let the empty word stand for [].

Signature. Let x be an object. [x] is a word.
Axiom. Let x,y be objects. (x,y) is a word.

Axiom. Let w be a word. Then (w = []) or (w = [x] for some object x) or (w = (x,y) for some objects
  x,y).

Let [u,v] stand for (u,[v]).
Let [u,v,w] stand for (u,v,[w]).
Let [u,v,w,x] stand for (u,v,w,[x]).
Let [u,v,w,x,y] stand for (u,v,w,x,[y]).
Let [u,v,w,x,y,z] stand for (u,v,w,x,y,[z]).


%
% Numbers
%

Signature. A number is a notion.
Signature. 0 is a number.
Signature. Let n be a number. The successor of n is a number.

Let succ(n) stand for the successor of n.

Let 1 stand for succ(0).
Let 2 stand for succ(1).

Definition. A nonzero number is a number that is not equal to 0.

Signature NatSum. Let n,m be numbers. n + m is a number.

Axiom Add0.    Let n be a number. n + 0 = n.
Axiom AddSucc. Let n,m be numbers. Then n + succ(m) = succ(n + m).
Axiom 0NoSucc. 0 is the successor of no number.
Axiom 0OrSucc. Every nonzero number is the successor of some number.

Definition DefLess. Let n,m be numbers. n is less than m iff m = n + k for some nonzero number k.

Let n < m stand for n is less than m.
Let n is greater than m stand for m is less than n.
Let n > m stand for n is greater than m.
Let n is less or equal m stand for n < m or n = m.
Let n /leq m stand for n is less or equal m.
Let n is greater or equal m stand for n > m or n = m.
Let n /geq m stand for n is greater or equal m.


%
% Length
%

Signature. Let w be a word. The length of w is a number.
Let length w stand for the length of w.

Axiom. The length of the empty word is equal to 0.
Axiom. Let x be an object. The length of [x] is equal to 1.
Axiom. Let x,y be objects. The length of (x,y) is equal to the successor of the length of y.

Definition. Let n be a number. A word of length n is a word w such that the length of w is equal to
  n.

[prove off]
Proposition. Let w be a word. If length(w) = 0 then w = [].

Proposition. Let w be a word. If length(w) = 1 then w = [x] for some object x.

Proposition. Let w be a word. If length(w) /geq 2 then w = (x,y) for some objects x,y.
[prove on]


%
% Concatenation
%

Signature. Let x be an object and w be a word. x : w is a word.

Axiom. Let x be an object. x : [] = [x].
Axiom. Let x,y,z be objects. x : (y,z) = (x,y,z).

Signature. Let v,w be words. v ++ w is a word.

Axiom. Let w be a word. [] ++ w = w.
Axiom. Let x be an object and w be a word. [x] ++ w = x : w.
Axiom. Let x,y be objects and w be a word. Assume that y is a word. (x,y) ++ w = x : (y ++ w).


%
% Formulas
%

Signature. A symbol is a notion.

Signature. A variable is a symbol.

Signature. 'forall' is a symbol.
Signature. 'exists' is a symbol.
Signature. 'not'    is a symbol.
Signature. 'and'    is a symbol.
Signature. 'or'     is a symbol.
Signature. 'imp'    is a symbol.
Signature. 'iff'    is a symbol.
Signature. 'lb'     is a symbol.
Signature. 'rb'     is a symbol.

Signature. A formula is a word.

Axiom. Let phi be a formula.
  ['not'] ++ phi is a formula.
Axiom. Let phi,psi be formulas.
  ['lb'] ++ (phi ++ (['rb'] ++ (['and'] ++ (['lb'] ++ (psi ++ ['rb']))))) is a formula.
Axiom. Let phi,psi be formulas.
  ['lb'] ++ (phi ++ (['rb'] ++ (['or'] ++ (['lb'] ++ (psi ++ ['rb']))))) is a formula.
Axiom. Let phi,psi be formulas.
  ['lb'] ++ (phi ++ (['rb'] ++ (['imp'] ++ (['lb'] ++ (psi ++ ['rb']))))) is a formula.
Axiom. Let phi,psi be formulas.
  ['lb'] ++ (phi ++ (['rb'] ++ (['iff'] ++ (['lb'] ++ (psi ++ ['rb']))))) is a formula.
Axiom. Let phi,psi be formulas and v be a variable.
  ['forall'] ++ ([v] ++ (['lb'] ++ (phi ++ (['rb'])))) is a formula.
Axiom. Let phi,psi be formulas and v be a variable.
  ['exists'] ++ ([v] ++ (['lb'] ++ (phi ++ (['rb'])))) is a formula.