Let a /in b stand for a is an element of b.
Let a /notin b stand for not a /in b.

Axiom. Let x,y be sets. If every element of x is a element of y and every element of y is an
element of x then x = y.

Definition. /emptyset = {u | u != u}.
Definition. Let x be a set. x is empty iff x = /emptyset.

Definition. Let x,y be sets. x /cup y = {u | u /in x or u /in y}.
Definition. Let x,y be sets. x /cap y = {u | u /in x and u /in y}.
Definition. Let x,y be sets. x /setminus y = {u | u /in x and u /notin y}.
Definition. Let x,y be sets. The symmetric difference of x and y is (x /cup y) /setminus (x /cap y).

Lemma. Let x,y,z be sets. x /cap (y /cup z) = (x /cap y) /cup (x /cap z).
Lemma. Let x be a set. x /cap x = x.
Lemma. Let x,y be sets. x = y iff the symmetric difference of x and y is empty.