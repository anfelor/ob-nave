Signature. S is a set.
Let f denote a function.
Let x denote an element of S.
Lemma. For all x f[x] is an element of S.
Proof. Obvious.
