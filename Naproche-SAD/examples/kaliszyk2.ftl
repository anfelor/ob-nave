[synonym number/-s]

Let x << y stand for x is an element of y.

Signature.
A NN is a notion.

Signature.
0 is a NN.

Signature.
Let x be a NN.
s(x) is a NN.

Definition. NNN is the set of NN.

Definition.
Let f be function.
Let A,B be sets.
f is from A to B iff Dom(f) = A and for every element x of A   f[x] is
an element of B.

Signature. P is a function from NNN to NNN.


Signature.
Let x,y be NN.
x + y is a NN.

Axiom.
for all NN w  0+w=w and for all NN t,v  s(t)+v=s(t+v).

Axiom.
  Let x be NN.
  x -<- s(x).

[prove on]

Theorem.
For every NN x  x+0 = s(x).
Proof by induction.
Let x be an NN.
Qed.

Theorem.   0 = s(0).
Proof. Qed. 
