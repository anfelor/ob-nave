[synonym number/-s]

Let x << y stand for x is an element of y.

Signature.
A NN is a notion.

Signature.
0 is a NN.

Signature.
Let x be a NN.
s(x) is a NN.

%Definition.
%Let f be a function.
%f is NNfunction iff for every x be a NN  f[x] is a NN.

Definition. NNN is the set of NN.

Definition.
Let f be function.
Let A,B be sets.
f is from A to B iff Dom(f) = A and for every element x of A   f[x] is
an element of B.

Signature. P is a function from NNN to NNN.
Theorem. 0 is element of NNN. Proof. Qed.
Theorem. P[0] is element of NNN. Proof. Qed.
Theorem. P[P[s(0)]] = P[P[s(0)]]. Proof. Qed.
Theorem. P[P[0]] is not equal to 0. Proof. Qed.
