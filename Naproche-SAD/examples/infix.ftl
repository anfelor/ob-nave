[synonym number/-s]

Signature. A real number is an object.
Let a,b denote a real numbers.
Let f denote a function.
Let a + b stand for f[a][b].
