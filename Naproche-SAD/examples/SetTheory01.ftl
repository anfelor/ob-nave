Definition 90.
Let x \in y stand for x is an element of y.
Let x \notin y stand for x is not an element of y.
Let x \neq y stand for x != y.

Let x,y,z,X,Y,Z stand for sets.
Definition 94a. The empty set is { set x | x \neq x}.
Let \emptyset stand for the empty set.

Definition 94b. The universe is { set x | x = x}.
Let \VV stand for the universe.

Proposition 95a. \emptyset \in \VV.

Definition 98a. A subset of Y is a set X such that
forall x (x \in X => x \in Y).
Let X \subset Y stand for X is a subset of Y.

Definition 98b. The union of X and Y is 
{set x | x \in X or x \in Y}.
Let X \cup Y stand for the union of X and Y.

Definition 98c. The intersection of X and Y is 
{set x | x \in X and x \in Y}.
Let X \cap Y stand for the intersection of X and Y.

Definition 98d. The difference of X and Y is 
{set x | x \in X and x \notin Y}.
Let X \setminus Y stand for the difference of X and Y.

Definition 98e. The union of X is
{set x | exists y (y \in X /\ x \in y)}.
Let \bigcup X stand for the union of X.

Definition 98f. The intersection of X is
{set x | forall y (y \in X => x \in y)}.
Let \bigcap X stand for the intersection of X.

Definition 98g. The power set of X is
{set x | x \subset X}.
Let \PP X stand for the power set of X.

Definition 98h. The singleton set of X is
{set x | x = X}.
Let <X> stand for the singleton set of X.

Axiom 99. X \subset Y /\ Y \subset X => X = Y.
