% 19.3 Class Terms


% Definition 90.
% QUESTION: Why does "Definition" have to be a comment?
Let x /in y stand for x is an element of y.
Let x /notin y stand for x is not an element of y.
Let x /neq y stand for x != y.

Let x,y,z,X,Y,Z stand for sets.

Axiom Ext1. Forall x,y (forall z (z /in x iff z /in y) => x = y).

Lemma Ext1Test. Let x be a set and X = {set y | y /in x}.
Then x = X.

% WARNING: Here we need X and can't check equality directly.
% Therefore the following axiom:

Axiom Ext2. Forall x (x = {set y | y /in x}).

% Lemma Ext2Test. Let x be a set. Then ({set y | y /in x} = x).
% WARNING: This works in Isabelle, but not with the Terminal.
% Last Output: "Maybe.fromJust: Nothing", then it stops.
% With this Lemma Isabelle has a problem in Line 137.

Definition 94a. The empty set is { set x | x /neq x}.
Let /emptyset stand for the empty set.

Definition. Let x be a set. x is empty iff x = /emptyset.
Definition. Let x be a set. x is nonempty iff exists y (y /in x).

Lemma. Let x be a set. x is nonempty iff x /neq /emptyset.

Lemma ESTest1. /emptyset is a set.
Lemma ESTest2. Forall y (y /notin /emptyset).
Lemma ESTest3. Let x be a set. If (forall y (y /notin x)) then x = /emptyset.

Definition 94b. The universe is { set x | x = x}.
Let /VV stand for the universe.

Proposition 95a. /emptyset /in /VV.

% WARNING: Naproche proofs
% Proposition 95b. /VV /in /VV.

Definition 98a. A subset of Y is a set X such that
forall x (x /in X => x /in Y).
Let X /subset Y stand for X is a subset of Y.

Definition 98b. The union of X and Y is 
{set x | x /in X or x /in Y}.
Let X /cup Y stand for the union of X and Y.

Definition 98c. The intersection of X and Y is 
{set x | x /in X and x /in Y}.
Let X /cap Y stand for the intersection of X and Y.

Definition 98d. The difference of X and Y is 
{set x | x /in X and x /notin Y}.
Let X /setminus Y stand for the difference of X and Y.

Definition 98e. The union of X is
{set x | exists y (y /in X /\ x /in y)}.
Let /bigcup X stand for the union of X.

Definition 98f. The intersection of X is
{set x | forall y (y /in X => x /in y)}.
Let /bigcap X stand for the intersection of X.

Definition 98g. The power set of X is
{set x | x /subset X}.
Let /PP X stand for the power set of X.

Definition 98h. The singleton set of X is
{set x | x = X}.
Let <X> stand for the singleton set of X.

Proposition 99. X /subset Y /\ Y /subset X => X = Y.

Proposition 100. Let Z = {X,Y}. Then /bigcup (Z) = X /cup Y.

% WARNING: We need Z

Lemma PairTest. Let x, y be sets and X = {x,y}. Then (z /in X <=> z = x \/ z = y).
Lemma PowersetTest1. Let x, y be sets and X = {x,y}. Then <x> /in /PP(X).
Lemma PowersetTest2. Let x be a set. Then /emptyset /in /PP(x).
Lemma PowersetTest3. Let x be a set. Then x /in /PP(x).
Lemma PowersetTest4. Let x, y be sets and X = {x,y}. Then /PP(X) = {/emptyset, <x>, <y>, X}.

Lemma SetTest1. Let x be a set. Then <x> = {x}.
Lemma SetTest2. Let x be a set. Then <x> = {x,x}.
Lemma SetTest2. Let x be a set. Then {x} = {x,x}.





% 20 Relations and functions



% 20.1 Ordered pairs and relations

Let a,b,c,d stand for sets.


Definition 101. Let x and y be sets. The ordered pair of x and y is 
{set z | z = <x> \/ (forall a (a /in z <=> a = x \/ a = y))}.
Let [x,y] stand for the ordered pair of x and y.

Lemma OPairTest1. Let x and y be sets. Then
[x,y] = {set z | z = <x> \/ (forall a (a /in z <=> a = x \/ a = y))}.


% QUESTION: Why can't we write "z = {x,y}"?

Lemma OPairTest2. Let x and y be sets. Then [x,y] is a set.

Axiom 103. Let x,y,X,Y be sets such that [x,y] = [X,Y]. Then x = X /\ y = Y.
% MISSING: Proof


Let A,B,R stand for sets.

Definition 105a. The cartesian product of A and B is
{set x | exists a exists b (x = [a,b] /\ a /in A /\ b /in B)}. 
Let A /times B stand for the cartesian product of A and B.

% WARNING: We wand this definition to hold also for class-sized A and B, especially for /VV.


Lemma CartTest1. Let x be an element of the cartesian product of A and B and x = [a,b]. Then a /in A /\ b /in B.

% WARNING: This works in Isabelle, with the Terminal we get a strange Error.

Lemma CartTest2. Let A and B be sets. Then A /times B = {[a,b] | a /in A /\ b /in B}.
% WARNING: This gives a strange error (in Isabelle):
% "src/SAD/Data/Structures/DisTree.hs:(75,1)-(77,61): Non-exhaustive patterns in function toStruct"
% But only in combination with the Lemma in line 22.
Lemma CartTest3. Let A and B be sets. Let x /in A /times B. Then exists a /in A exists b /in B (x = [a,b]).
Lemma CartTest4. Let a /in A and b /in B. Then [a,b] /in A /times B.
Lemma CartTest5. Let a /in A and b /in B. Then [a,b] /subset /PP (A /cup B).
% WARNING: The Terminal stops with these lemmata and gives the Error 
% "src/SAD/Prove/Normalize.hs:(186,1)-(193,50): Non-exhaustive patterns in function listEq"

[synonym relation/-s]
Definition. A relation is a set R such that 
R /subset /VV /times /VV.
Let a - R - b stand for [a,b] /in R.



Let R,S stand for relations.

Definition 106a. The domain of R is
{set x | exists y ([x,y] /in R)}.
Let domain(R) stand for the domain of R.

Definition 106b. The range of R is
{set y | exists x ([x,y] /in R)}.
Let ran(R) stand for the range of R.

Definition 106c. The field of R is
domain(R) /cup ran(R).
Let field(R) stand for the field of R.

Definition 106d. The restriction of R to A is
{set z | z /in R /\ exists x exists y (z = [x,y] /\ x /in A)}.
Let R /upharpoonright A stand for the restriction of R to A.

Definition 106e. The image of A under R is
{set y | exists z exists x (x /in A /\ z = [x,y] /\ z /in R)}.
Let R[A] stand for the image of A under R.

% Definition 106f. The preimage of B under R is
% {set x | exists z exists y (y /in B /\ z = [x,y] /\ z /in R)}.
% Let R^{-1}[B] stand for the preimage of B under R.

% WARNING: Potential conflict if we later define R^{-1}

Definition 106g. The composition of S and R is
{set u | exists x exists y exists z (x - R - y /\ y - S - z /\ u = [x,z])}.
Let S /circ R stand for the composition of S and R.

Definition 106h. The inverse of R is
{set a | exists x exists y (a = [y,x] /\ [x,y] /in R)}.
Let R^{-1} stand for the inverse of R.

Lemma RelTest1. domain(/emptyset) = /emptyset.
Lemma RelTest2. Let R be a relation. Let x /in R and x = [a,b]. Then [b,a] /in R^{-1}.
Lemma RelTest3. Let x /in R^{-1} and x = [a,b] and y = [b,a]. Then y /in R.
Lemma RelTest4. Let R be a relation. Then R /subset /VV /times /VV.
Lemma RelTest5. Let R be a relation. Then R^{-1} is a relation.

Lemma RelTest6. Let R be a relation. Then (R^{-1})^{-1} is a relation.
Lemma RelTest7. Let x /in R. Then exists y exists z (x = [y,z]).
Lemma RelTest8. Let R be a relation. Let x /in R^{-1}.
Then exists y exists z ([z,y] /in R /\ x = [y,z]).
Lemma RelTest9. Let R be a relation. Let x /in (R^{-1})^{-1}.
Then exists y exists z ([z,y] /in R^{-1} /\ x = [y,z]).
Lemma RelTest10. Let R be a relation. Let x /in (R^{-1})^{-1}.
Then exists y exists z ([y,z] /in R /\ x = [y,z]).

% WARNING: This does not work with "x /in R"

Lemma RelTest11. Let R be a relation. Let x /in R. Let x = [y,z]. Then [y,z] /in R.
Lemma RelTest12. Let R be a relation. Let x /in (R^{-1})^{-1}.
Then exists y exists z (x /in R /\ x = [y,z]).
% Proof. Take y, z such that ([y,z] /in R /\ x = [y,z]).
% Then x /in R /\ x = [y,z].
% qed.

Lemma RelTest13. (R^{-1})^{-1} /subset R.

% FunFact: We needed only RelTest5 for this


Definition 107a. R is reflexive iff 
forall x (x /in field(R) => x - R - x).

Definition 107b. R is irreflexive iff 
forall x (x /in field(R) => not x - R - x).

Definition 107c. R is symmetric iff
forall x, y (x - R - y => y - R - x).

Definition 107d. R is antisymmetric iff
forall x, y (x - R - y /\ y - R - x => x = y).

Definition 107e. R is transitive iff
forall x, y, z (x - R - y /\ y - R - z => x - R - z).

Definition 107f. R is connex iff
forall x, y (x,y /in field(R) => x - R - y \/ y - R - x \/ x = y).

Definition 107g. An equivalence relation is a relation R
such that R is reflexive and R is symmetric and R is transitive.

Definition 107h. Let R be an equivalence relation.
The equivalence class of x modulo R is
{set y | y - R - x}.
Let [x]{R} denote the equivalence class of x modulo R.

Definition 108a.
A partial order is a relation R such that
R is reflexive and R is transitive and R is antisymmetric.

Definition 108b.
A linear order is a relation R such that R is connex and
R is a partial order.

Definition 108c. A partial order on A is a partial order R such
that field(R) = A.

Definition 108d. A strict partial order is a transitive relation
that is irreflexive.

Definition 108e. A strict linear order is a connex 
strict partial order.




% 20.2 Functions

Let F denote a relation.

Definition 109. A map is a relation F such that
forall x,y,z (x - F - y /\ x - F - z => y = z).

Definition 109a. Let F be a map. The value of F at x is 
{set u | forall y (x - F - y => u /in y)}.
Let F(x) denote the value of F at x.

Lemma ValTest1. Let F be a map. Let x /in domain(F). Let x - F - y. Then y = F(x).
Lemma ValTest2. Let F be a map. Let x /in domain(F). Let y = F(x). Then x - F - y.
Proof. Take z such that x - F - z. z = y.
qed.

Let F,A,B denote sets.

Definition 110a. A map from A to B is a relation F such that
F is a map and domain(F) = A and ran(F) /subset B.
Let F : A /rightarrow B stand for F is a map from A to B.

Definition 110b. A partial map from A to B is a relation F such that
F is a map and domain(F) /subset A and ran(F) /subset B.
Let F : A /harpoonright B stand for F is a partial map from A to B.

Definition. Id = {set x | exists y /in /VV (x = [y,y])}. 

% NOTE: We do not need "/in VV", even for the following lemmata.

Lemma IdTest1. Id is a relation.
Lemma IdTest2. Id is a map.
% FunFact: We actually need IdTest1 for this.
Lemma IdTest3. domain(Id) = /VV.
Lemma IdTest4. ran(Id) = /VV.
Lemma IdTest5. Id(x) = x.

Lemma IdTest6. Id : /VV /rightarrow /VV.

Definition. Assume F : A /rightarrow B. F is surjective from A to B
iff ran(F) = B.

Lemma IdTest7. Id is surjective from /VV to /VV.

Definition 110d. Let F be a map. F is injective iff
forall x,y (x,y /in domain(F) /\ x /neq y => F(x) /neq F(y)).

Lemma IdTest8. Id is injective.
Lemma IdTest9. Id^{-1} = Id.

Definition 110e. A bijective map from A to B is a map F such that
F : A /rightarrow B and F is surjective from A to B 
and F is injective.
Let F : A /leftrightarrow B stand for 
F is a bijective map from A to B.

Lemma IdTest10. Id : /VV /leftrightarrow /VV.

Lemma BijTest1. Let F be a bijective map from A to B. Then F^{-1} is a relation.
Lemma BijTest2. Let F be a bijective map from A to B. Then domain(F^{-1}) = B and ran(F^{-1}) = A.
Lemma BijTest3. Let F be a bijective map from A to B.
Then forall x,y (x,y /in domain(F) /\ F(x) = F(y) => x = y).
Lemma BijTest4. Let F be a bijective map from A to B.
Then forall x,y,z (x,y /in domain(F) /\ x - F - z /\ y - F - z => x = y).
Lemma BijTest5. Let F be a bijective map from A to B. Then F^{-1} is a map from B to A.

Definition 110f. ^{A}B = {set f | f : A /rightarrow B}.

Lemma IdTest11. Id /in ^{/VV}/VV.

% WARNING: We restricted our definition to set-sized A, B. We have to be careful when we have implemented that /VV is not a set.


Lemma InjTest1. Let F be an injective map. Then F^{-1} is a map.
Lemma InjTest2. Let F be an injective map such that F: A /rightarrow B.
Then F^{-1} is a map and domain(F^{-1}) /subset B and ran(F^{-1}) = A.
Lemma InjTest3. Let F be an injective map such that F: A /rightarrow B.
Then F^{-1} is a partial map from B to A.
Lemma InjTest4. Let F be an injective map such that F: A /rightarrow B. Let C = ran(F).
Then F^{-1} is a map from C to A.
Lemma InjTest5. Let F be an injective map such that F: A /rightarrow B. Let C = ran(F).
Then F is a bijective map from A to C.

Lemma InjTest6. Let F be an injective map such that F: A /rightarrow B. Let C = ran(F).
Then F^{-1} is a map from C to A.



% 21 Various results


Proposition 114. A /times B /subset /PP( /PP(A /cup B)).


Theorem 115a. There exists an injective map F such that F : A /rightarrow /PP A.
Proof. Define F = {[u,<u>] | u /in A}. F is a map. domain(F) = A.
ran(F) /subset /PP A.
F is injective.
qed.


Theorem 115b. Let A be a nonempty set. There is no injective map G such that 
G: /PP A /rightarrow A.
Proof. Assume the contrary.
Take an injective map G such that G : /PP A /rightarrow A.
G^{-1} is a map from ran(G) to /PP A.
Define C = {set u | u /in A /\ u /notin G^{-1}(u)}.
C /subset A. C /in ran(G^{-1}).
Take a set y such that y - G^{-1} - C. Then y /in C iff y /notin G^{-1}(y).
y /in C iff y /notin C.
Contradiction.
qed.








