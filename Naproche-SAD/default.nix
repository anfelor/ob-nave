{ mkDerivation, base, bytestring, containers, eprover, ghc-prim
, hpack, mtl, network, process, split, stdenv, text, threads, time
, transformers, utf8-string, uuid, yaml
}:
mkDerivation {
  pname = "Naproche-SAD";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    base bytestring containers ghc-prim mtl network process split text
    threads time transformers utf8-string uuid yaml
  ];
  librarySystemDepends =  [ eprover ];
  libraryToolDepends = [ hpack ];
  executableHaskellDepends = [
    base bytestring containers ghc-prim mtl network process split text
    threads time transformers utf8-string uuid yaml
  ];
  prePatch = "hpack";
  homepage = "https://github.com/Naproche/Naproche-SAD#readme";
  license = stdenv.lib.licenses.gpl3;
}
